import { createAction, props } from '@ngrx/store';

export const initDeck = createAction('[Table] Initialize Deck');

export const revealCard = createAction('[Table] Reveal Card', props<{ uuid: string }>());

export const hideCards = createAction('[Table] Hide Cards');

export const pairFound = createAction('[Table] Pair Found', props<{ uuids: string[] }>());
