import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { EMPTY, of } from "rxjs";
import { delay, exhaustMap, withLatestFrom } from "rxjs/operators";
import { hideCards, pairFound, revealCard } from "./app.actions";
import { State } from "./app.reducer";
import { selectRevealedCardCount, selectRevealedCards } from "./app.selectors";

export const flipTime = 0.2;

@Injectable()
export class AppEffects {
  constructor(private actions$: Actions, private store: Store<State>) {}

  revealCard$ = createEffect(() =>
    this.actions$.pipe(
      ofType(revealCard),
      withLatestFrom(
        this.store.select(selectRevealedCards),
        this.store.select(selectRevealedCardCount)
      ),
      exhaustMap(([_, revealedCards, revealedCardCount]) => {
        if (revealedCardCount > 1) {
          const card1 = revealedCards[0];
          const card2 = revealedCards[1];
          return card1.id === card2.id
            ? of(pairFound({ uuids: [card1.uuid, card2.uuid] })).pipe(
                delay((flipTime * 1000) / 2)
              )
            : of(hideCards()).pipe(delay(400));
        } else {
          return EMPTY;
        }
      })
    )
  );
}
