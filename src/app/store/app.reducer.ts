import { Action, ActionReducerMap, createReducer, on } from "@ngrx/store";
import { Card } from "../models/card.model";
import { hideCards, initDeck, revealCard, pairFound } from "./app.actions";
import { generateDeck } from "./util";

export const deckFeatureStateKey = "deck";

export interface State {
  deck: DeckState;
}

export interface DeckState {
  deck: Card[];
  revealedCardCount: number;
  numberOfTries: number;
}

const initialState: DeckState = {
  deck: [],
  revealedCardCount: 0,
  numberOfTries: 0,
};

const gameReducer = createReducer(
  initialState,
  on(initDeck, () => ({ ...initialState, deck: generateDeck(20) })),
  on(revealCard, (state, props) => reveal(state, props)),
  on(hideCards, (state) => hide(state)),
  on(pairFound, (state, props) => pair(state, props))
);

export function reducer(state: DeckState | undefined, action: Action) {
  return gameReducer(state, action);
}

export const reducers: ActionReducerMap<State> = {
  deck: reducer,
};

function reveal(state: DeckState, props: { uuid: string }): DeckState {
  if (state.revealedCardCount === 2) {
    return state;
  }
  const card = state.deck.find((card) => card.uuid === props.uuid);
  if (card.found || card.revealed) {
    return state;
  }
  return {
    ...state,
    revealedCardCount: state.revealedCardCount + 1,
    numberOfTries:
      state.revealedCardCount > 0
        ? state.numberOfTries + 1
        : state.numberOfTries,
    deck: state.deck.map((card) =>
      props.uuid === card.uuid ? { ...card, revealed: true } : card
    ),
  };
}

function hide(state: DeckState): DeckState {
  return {
    ...state,
    revealedCardCount: 0,
    deck: state.deck.map((card) =>
      !card.found && card.revealed ? { ...card, revealed: false } : card
    ),
  };
}

function pair(state: DeckState, props: { uuids: string[] }): DeckState {
  return {
    ...state,
    revealedCardCount: 0,
    deck: state.deck.map((card) =>
      props.uuids.includes(card.uuid) ? { ...card, found: true } : card
    ),
  };
}
